# README #

Puppeteer testing environment for Lerning Puppeteer EDU

### What is this repository for? ###

* Quick summary
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

#### Puppeteer testing environment setup notes ###

* clone repo
* execute in terminal [ npm i ] from root folder
* execute in terminal [ node location/testName ] from root folder if test does not implement Mocha, to run test
* execute in terminal [ npm run location/testName ] from root folder if test implements Mocha and is registered in 
package.json, to run test

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
* View tests here: https://drive.google.com/drive/folders/1OVi3QJPWDFYEhcYQZUNXkfrWfZvcxLcd?usp=sharing


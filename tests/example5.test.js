const puppeteer = require('puppeteer')
const expect = require('chai').expect

let browser
let page

before(async () => {
    browser = await puppeteer.launch({
        headless: false,
        slowMo: 200,
        args: [
            '--start-fullscreen'
        ],
    })
    page = await browser.newPage()
    await page.setViewport({
        width: 1920 * .75,
        height: 1080
    })
})

describe('My First Puppeteer Test', () => {
    it('Going to homepage', async function() {
        await page.goto('https://devexpress.github.io/testcafe/example/')

        await page.waitForSelector('#developer-name')
    })

    it('Filling the Name field', async function() {
        await page.type('#developer-name', 'Developer')

        await page.waitForSelector('#remote-testing')
    })

    it('Clicking checkbox', async function() {
        await page.click('#remote-testing')

        await page.waitForSelector('#preferred-interface')
    })

    it('Choosing an option from the dropdown', async function() {
        await page.select('#preferred-interface', 'JavaScript API')

        await page.waitForSelector('#submit-button')
    })

    it('Submitting the form', async function() {
        await page.click('#submit-button')

        await page.waitForSelector('#article-header')
    })

    it('Asserting link in form submission success page', async function() {
        const link = await page.$eval('body > div > p > a', element => element.textContent)
        expect(link).to.be.equal('devexpress.github.lv/testcafe', 'Placeholder message: assertion went wrong :( ')
    })

    it('Waiting 3 seconds before closing the browser', async function() {
        await page.waitForTimeout(3000)
    })
})

after(async () => {
    await browser.close()
})

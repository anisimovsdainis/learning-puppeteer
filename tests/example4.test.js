const puppeteer = require('puppeteer');

(async () => {
    const browser = await puppeteer.launch({
        headless: false,
        slowMo: 200,
        args: [
            '--start-fullscreen'
        ],
    })
    const page = await browser.newPage()
    await page.setViewport({
        width: 1920 * .75,
        height: 1080
    })
    await page.goto('https://devexpress.github.io/testcafe/example/')

    await page.waitForSelector('#developer-name')
    await page.type('#developer-name', 'Developer')

    await page.waitForSelector('#remote-testing')
    await page.click('#remote-testing')

    await page.waitForSelector('#preferred-interface')
    await page.select('#preferred-interface', 'JavaScript API')

    await page.waitForSelector('#submit-button')
    await page.click('#submit-button')

    await page.waitForSelector('#article-header')

    await page.waitForTimeout(3000)
    await browser.close()
})()

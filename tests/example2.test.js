const puppeteer = require('puppeteer');

(async () => {
    const browser = await puppeteer.launch({
        headless: false,
        slowMo: 200,
        args: [
            '--start-fullscreen'
        ],
    })
    const page = await browser.newPage()
    await page.setViewport({
        width: 1920 * .75,
        height: 1080
    })
    await page.goto('https://devexpress.github.io/testcafe/example/')
    await page.waitForTimeout(3000)
    await browser.close()
})()

const puppeteer = require('puppeteer');

(async () => {
    const browser = await puppeteer.launch()
    const page = await browser.newPage()
    await page.goto('https://devexpress.github.io/testcafe/example/')
    await browser.close()
})()
